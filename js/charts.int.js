// 商品存货周转天数
var formatUtil = echarts.format;
var turnaround = echarts.init(document.getElementById('turnaround'));
$.get("data/各类商品存货周转天数.json").done(function (data) {
    //data = JSON.parse(data),
    turnaround.setOption({
        tooltip:{
        formatter: '{b}：{c}'
    },
    series: [{
        type: 'treemap',
        label:{
            show:true,
            position:'insideTopLeft',
            distance:0,
            padding:10,
            formatter:"{b}\n{a|{c}}",
            rich: {
                a: {
                    padding:6,
                    align:'right',
                    verticalAlign:'bottom',
                    color:'#fff',
                }
            }
        },
        roam:false,
        nodeClick:false,
        breadcrumb:{show:false},
        itemStyle:{
            gapWidth:5,
            borderColor:'transparent'
        },
        data: data.data
    }]
        })
});



// 存销量
var stockSales = echarts.init(document.getElementById('stockSales'));
$.get("data/商品库存数量和销售数量.json").done(function (data) {
    //data = JSON.parse(data),
	stockSales.setOption({
	//在这里完成代码
		})
});



// 滞销商品
var unsalable = echarts.init(document.getElementById('unsalable'));
$.get("data/商品滞销数据.json").done(function (data) {
	//data = JSON.parse(data),
	unsalable.setOption({
    //在这里完成代码
		})
});



//品类库存占比 
var categoryStock = echarts.init(document.getElementById('categoryStock'));
$.get("data/不同类型的商品库存数量.json").done(function (data) {
	//data = JSON.parse(data),
	categoryStock.setOption({
    //在这里完成代码
		})
});



// 设备容量
var mVolume = echarts.init(document.getElementById('mVolume'));
$.get("data/不同地点售货机库存数量和缺货数量.json").done(function (data) {
	//data = JSON.parse(data),
	mVolume.setOption({
    //在这里完成代码
		})
});



window.onresize = function() {
    turnaround.resize();
    stockSales.resize();
    unsalable.resize();
    categoryStock.resize();
    mVolume.resize();
}