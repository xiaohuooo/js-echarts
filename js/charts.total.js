// 销售金额 & 订单量 & 毛利润 & 售货机数量 & 购买用户数
$.get("data/无人售货机各特征数据.json").done(function (data) {
    //data = JSON.parse(data);
    saleT('saleM', '销售金额', 0, data.销售金额[0], data.销售金额[1], data.销售金额[2], '','#1779d9','rgba(23,121,217,0.6)');
    saleT('orderQ', '订单量', 0, data.订单量[0], data.订单量[1], data.订单量[2], '','#30b761','rgba(48,183,97,0.5)');
    saleT('grossM', '毛利润', 0, data.毛利润[0], data.毛利润[1], data.毛利润[2], '','#d04a4b','rgba(208,74,75,0.5)');
    saleT('discount', '折扣额', 0, data.折扣额[0], data.折扣额[1], data.折扣额[2], '千','#ca841e','rgba(202,132,30,0.5)');
    saleT('unitP', '客单价', 0, data.客单价[0], data.客单价[1], data.客单价[2], '','#00a7c2','rgba(0,167,194,0.5)');
});
/*
 *id: chart容器id;
 *title: 仪表盘名称
 *min: 最小值
 *max: 最大值
 *val: 当前实际值
 *tag: 目标值
 *unit: 单位符号
 *color1: 主轴颜色
 */

var saleM = echarts.init(document.getElementById("saleM"));
var orderQ = echarts.init(document.getElementById("orderQ"));
var grossM = echarts.init(document.getElementById("grossM"));
var discount = echarts.init(document.getElementById("discount"));
var unitP = echarts.init(document.getElementById("unitP"));

function saleT(id, title, min, max, val, tag, unit, color1,  color2) {

    var myChart = echarts.init(document.getElementById(id));

    option = {
        tooltip: {
            confine:true,
            trigger: 'item',
            formatter:function(data){
                hbl = (data.value/tag).toFixed(2);
                return title + "：" + data.value + '<br/>' + name + "：" + hbl
            }
        },
        series: [{
            startAngle: 180, 
            endAngle: 0,
            splitNumber: 1,
            name: title,
            type: 'gauge',
            radius: '100%',
            axisLine: {
                lineStyle: {
                    color: [
                        [0.25, '#1779da'],
                        [0.5, '#1779da'],
                        [1, '#ddd']
                    ],
                    width: 20
                }
            },
            axisTick: { show: false },
            axisLabel: { 
                distance:0,
                width:30,
                height:24,
                lineHeight:24,
                padding:[25,-30,0],
                color:'rgba(255,255,255,0.5)',
                formatter: function (value) {
                    if(unit=='千'){
                        return (value/1000).toFixed(1) + ' ' + unit;
                    }else if(unit=='万'){
                        return (value/10000).toFixed(1) + ' ' + unit;
                    }else{
                        return value;
                    }
                }
            },
            splitLine: { show: false },
            pointer: { show:false, width: 3 },
            title: {
                offsetCenter: [0, '92%'],
                color:'rgba(255,255,255,0.7)'
            },
            detail: {
                offsetCenter: [0, '-10%'],
                formatter: function(value){
                    value1 = value / tag;
                    return '{a|' + value.toFixed(1) + '}';
                },
                rich: {
                    a: {
                        fontSize:'16',
                        fontWeight:'bold'
                    }
                }
            },
            data: [{}]
        }]
    };
    option.series[0].min = min;
    option.series[0].max = max;
    option.series[0].data[0].value = val;
    option.series[0].axisLine.lineStyle.color[0][0] = (tag - min) / (max - min);
    option.series[0].axisLine.lineStyle.color[0][1] = color2;
    option.series[0].axisLine.lineStyle.color[1][0] = (val - min) / (max - min);
    option.series[0].axisLine.lineStyle.color[1][1] = color1;

    myChart.setOption(option);

}



//销售金额变化趋势
//初始化图表
var saleRate = echarts.init(document.getElementById('saleRate'));
//设置图表option值
$.get("data/售货机销售金额及其环比增长率.json").done(function (data) {
    //data = JSON.parse(data);
	saleRate.setOption({
    //在这里完成代码
		})
});



//商品销售金额Top5
var saleMtop5 = echarts.init(document.getElementById('saleMtop5'));
$.get("data/商品销售金额前5名.json").done(function (data) {
    //data = JSON.parse(data);
	saleMtop5.setOption({
    //在这里完成代码
		})
});



//售货机销售情况
var saleOrder = echarts.init(document.getElementById('saleOrder'));
$.get("data/不同地点售货机销售数据.json").done(function (data) {
    //data = JSON.parse(data);
	saleOrder.setOption({
    //在这里完成代码
		})
});



//支付方式占比
var payWay = echarts.init(document.getElementById('payWay'));
$.get("data/不同支付方式用户人数.json").done(function (data) {
    //data = JSON.parse(data);
	payWay.setOption({
    //在这里完成代码
		})
});



window.onresize = function() {
    saleM.resize();
    orderQ.resize();
    grossM.resize();
    discount.resize();
    unitP.resize();
    saleRate.resize();
    saleMtop5.resize();
    saleOrder.resize();
    payWay.resize();
};