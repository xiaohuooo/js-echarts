// 近5日用户人数新增和流失趋势
var lossGrowth = echarts.init(document.getElementById('lossGrowth'));
$.get("data/近5日新增和流失用户数据.json").done(function (data) {
    //data = JSON.parse(data);
	lossGrowth.setOption({
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            type:'scroll'
        },
        grid: {
            left: '10',
            right: '30',
            bottom: '10',
            containLabel: true
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: data.日期
        },
        yAxis: {
            type: 'value'
        },
        series: [
            {
                name:'新增人数',
                type:'line',
                data:data.新增人数,
                symbol:'circle',
                areaStyle: {
                    normal: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgba(194, 53, 49,.8)'
                        }, {
                            offset: 1,
                            color: 'transparent'
                        }])
                    }
                }
            },
            {
                name:'流失人数',
                type:'line',
                symbol:'circle',
                data:data.流失人数,
                areaStyle: {
                    normal: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgba(47, 69, 84,.8)'
                        }, {
                            offset: 1,
                            color: 'transparent'
                        }])
                    }
                }
            }
        ]
	});
});



//用户类型人数
var cSorNum = echarts.init(document.getElementById('cSorNum'));
$.get("data/不同类型用户的人数.json").done(function (data) {
    //data = JSON.parse(data);
	cSorNum.setOption({
    //在这里完成代码
		})
});



// 用户分群
var userGroup = echarts.init(document.getElementById('userGroup'));
$.get("data/用户分群数据.json").done(function (data) {
    //data = JSON.parse(data);
	userGroup.setOption({
    //在这里完成代码
		})
});



// 用户消费时段
var expTime = echarts.init(document.getElementById('expTime'));
$.get("data/用户消费时段数据.json").done(function (data) {
    //data = JSON.parse(data);
	expTime.setOption({
    //在这里完成代码
		})
});



// 用户消费地点
var expLoc = echarts.init(document.getElementById('expLoc'));
$.get("data/用户消费地点数据.json").done(function (data) {
    //data = JSON.parse(data),
	expLoc.setOption({
    
		})
});



// 商品价格区间
var pInterval = echarts.init(document.getElementById('pInterval'));
pInterval.setOption({
    grid: {
        left: '3%',
        right: '10',
        bottom: '10',
        containLabel: true
    },
    tooltip : {
        showDelay : 0,
        formatter : function (params) {
                return params.seriesName + '<br/>' + '单价：' + params.value[0] + '<br/>' + '销量：' + params.value[1];
        },
        axisPointer:{
            show: true,
            type : 'cross',
            lineStyle: {
                type : 'dashed',
                width : 1
            }
        }
    },

    legend: {
        type:'scroll',
    },
    xAxis :{ scale:true},
    yAxis :{ scale:true},
});

$.get("data/商品销量数量和价格数据.json").done(function (data) {
    //data = JSON.parse(data);
    var series=[];
    for(var i = 0;i < data.data.length;i++){
        series.push({
            name: data.data[i].name,
            type: 'scatter',
            data: [data.data[i].value],
            symbolSize:data.data[i].value[1]*2
        });
    }
    pInterval.setOption({
        series:series
    });
});



// 用户画像
var chart = echarts.init(document.getElementById('userHot'));
$.get("data/用户购买的商品名称和商品数量数据.json").done(function (data) {
    //data = JSON.parse(data);
    var option = {
        
        //在这里完成代码

    };
    chart.setOption(option);
});




window.onresize = function() {
    cSorNum.resize();
    lossGrowth.resize();
    expTime.resize();
    expLoc.resize();
    pInterval.resize();
    userGroup.resize();
    chart.resize();
};